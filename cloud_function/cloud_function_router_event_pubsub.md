
```
const { PubSub } = require('@google-cloud/pubsub');

// process event
exports.handleEventarcEvent = async (event) => {
  const pubSubClient = new PubSub();
  const topicName = 'projects/project-id/topics/your-topic';

  try {
    // extract body message
    const data = extractEventData(event);

    // validate body message
    validateMessageData(data);

    // Create y send message to Pub/Sub
    const messageData = {
      id: data.id,
      bucketName: data.bucket,
      fileName: data.name,
    };

    console.log('Preparando para publicar el mensaje:', messageData);
    await publishMessage(pubSubClient, topicName, messageData);
    console.log('Mensaje publicado exitosamente en Pub/Sub:', messageData);

  } catch (error) {
    console.error('Error procesando el evento:', error.message);
    throw new Error('Falló el procesamiento del evento');
  }
};

// Extraer el cuerpo del mensaje del evento
const extractEventData = (event) => {
  if (!event || !event.body) {
    throw new Error('Evento inválido: falta la propiedad "body".');
  }

  return event.body;
};

// Validar los datos extraídos del cuerpo del mensaje
const validateMessageData = (data) => {
  const { bucket, name, id } = data;

  validateNotEmpty(bucket, 'bucket');
  validateNotEmpty(name, 'name');
  validateNotEmpty(id, 'id');
};

// Validador genérico para propiedades no vacías
const validateNotEmpty = (value, propertyName) => {
  if (!value) {
    throw new Error(`Datos inválidos: falta "${propertyName}" en el cuerpo del mensaje.`);
  }
};


// Publicar el mensaje en Pub/Sub
const publishMessage = async (pubSubClient, topicName, data) => {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await pubSubClient.topic(topicName).publish(dataBuffer);
    console.log('Mensaje publicado con éxito');
  } catch (error) {
    console.error('Error al publicar en Pub/Sub:', error.message);
    throw new Error('Error al publicar en Pub/Sub: ' + error.message);
  }
};
```
