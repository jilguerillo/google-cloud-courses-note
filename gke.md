# Google-cloud-courses-note

### Solicitar el nombre de la cuenta activa con este comando
```
gcloud auth list
```

### Solicitar el ID del proyecto con este comando
```
gcloud config list project
```

### Configura la región de procesamiento predeterminada
```
gcloud config set compute/region "REGION"
```

### Configura la zona de procesamiento predeterminada
```
gcloud config set compute/zone "ZONE"
```

## GKE

### Crea clouster
```
gcloud container clusters create --machine-type=e2-medium --zone=ZONE lab-cluster
```
### or
```
`gcloud container clusters create io`
```

### Use the following command to see the cluster's status:
```
`gcloud container clusters list`
```

### Once your cluster has RUNNING status, get the cluster credentials:
```
`gcloud container clusters get-credentials day2-ops --region europe-west1`
```

#### Obtén el código de muestra - Copia el código fuente desde la línea de comandos
```
`gsutil cp -r gs://spls/gsp021/* .`
```

### iniciar una sola instancia del contenedor nginx:
```
`kubectl create deployment nginx --image=nginx:1.10.0`
```

### Usa el comando kubectl get pods para ver el contenedor nginx en ejecución
```
`kubectl get pods`
```

### Get the external IP of the application. you may need to repeat the commands until there's an external IP address assigned:
```
export EXTERNAL_IP=$(kubectl get service frontend-external -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
echo $EXTERNAL_IP
```
### Confirm that the app is up and running:
```
`curl -o /dev/null -s -w "%{http_code}\n"  http://${EXTERNAL_IP}`
```

### Una vez que el contenedor nginx alcance el estado Running, podrás exponerlo por fuera de Kubernetes con el comando kubectl expose
###  En segundo plano, Kubernetes creó un balanceador de cargas externo vinculado a una dirección IP pública. Los clientes que accedan a esa dirección IP pública serán redirigidos a los Pods del servicio. En este caso, esto correspondería al Pod de nginx
```
kubectl expose deployment nginx --port 80 --type LoadBalancer
```

### Lista de nuestros servicios con el comando kubectl get services:
```
kubectl get services
```

### Acceder al contenedor Nginx de forma remota, agrega la IP externa a este comando
```
curl http://<External IP>:80
```

---------------------------------------------------------------------------------------------------------------------
### ARCHIVO CONFIGURACIÓN DE UN pod monolith.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: monolith
  labels:
    app: monolith
spec:
  containers:
    - name: monolith
      image: kelseyhightower/monolith:1.0.0
      args:
        - "-http=0.0.0.0:80"
        - "-health=0.0.0.0:81"
        - "-secret=secret"
      ports:
        - name: http
          containerPort: 80
        - name: health
          containerPort: 81
      resources:
        limits:
          cpu: 0.2
          memory: "10Mi"
```

---------------------------------------------------------------------------------------------------------------------

### Crear el Pod de la aplicación monolítica
```
kubectl create -f pods/monolith.yaml
```

### Para obtener más información sobre el Pod de la aplicación monolítica, usa el comando kubectl describe cuando se ejecute el Pod
```
kubectl describe pods monolith
```

## Interactúa con los Pods

### Para configurar la redirección de puertos
```
kubectl port-forward monolith 10080:80
```

### Intenta acceder para que la aplicación monolítica proporcione un token de autenticación pass:password
```
curl -u user http://127.0.0.1:10080/login
```

#### Crea una variable de entorno para el token
```
TOKEN=$(curl http://127.0.0.1:10080/login -u user|jq -r '.token')
```

#### Usa este comando para copiar y usar el token con el objetivo de acceder el extremo seguro con curl
```
curl -H "Authorization: Bearer $TOKEN" http://127.0.0.1:10080/secure
```

---------------------------------------------------------------------------------------------------------------------

### Usa el comando kubectl exec para ejecutar una shell interactiva dentro del Pod. ejemplo usar ping -c 3 google.com
```
kubectl exec monolith --stdin --tty -c monolith -- /bin/sh
```

*********************************************************************************************************************

## Crea un Service

### Crea los Pods y sus datos de configuración con este comando
```
kubectl create secret generic tls-certs --from-file tls/
kubectl create configmap nginx-proxy-conf --from-file nginx/proxy.conf
kubectl create -f pods/secure-monolith.yaml
```

#### monolith.yaml
---------------------------------------------------------------------------------------------------------------------
```
kind: Service
apiVersion: v1
metadata:
  name: "monolith"
spec:
  selector:
    app: "monolith"
    secure: "enabled"
  ports:
    - protocol: "TCP"
      port: 443
      targetPort: 443
      nodePort: 31000
  type: NodePort
```
---------------------------------------------------------------------------------------------------------------------

### comando kubectl create para crear el Service de la aplicación monolítica a partir del archivo de configuración del Service de la aplicación
```
kubectl create -f services/monolith.yaml
```

### comando gcloud compute firewall-rules para permitir el tráfico hacia el Service de la aplicación monolítica en el puerto del nodo expuesto
```
gcloud compute firewall-rules create allow-monolith-nodeport \
  --allow=tcp:31000
```


### ver etiquetas a los Pods
```
kubectl get pods -l "app=monolith"
kubectl get pods -l "app=monolith,secure=enabled"
```

### Agrega etiquetas a los Pods
```
kubectl label pods secure-monolith 'secure=enabled'
kubectl get pods secure-monolith --show-labels
```

###  observa la lista de extremos en el Service de la aplicación monolítica con este comando
```
kubectl describe services monolith | grep Endpoints
```

---------------------------------------------------------------------------------------------------------------------

## Crea Deployments
### Dividirás la aplicación monolítica en tres partes:

    auth: Genera tokens JWT para usuarios autenticados.
    hello: Saluda a los usuarios autenticados.
    frontend: Enruta el tráfico hacia los servicios auth y hello

    
#### archivo de configuración del Deployment auth
---------------------------------------------------------------------------------------------------------------------
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth
spec:
  selector:
    matchlabels:
      app: auth
  replicas: 1
  template:
    metadata:
      labels:
        app: auth
        track: stable
    spec:
      containers:
        - name: auth
          image: "kelseyhightower/auth:2.0.0"
          ports:
            - name: http
              containerPort: 80
            - name: health
              containerPort: 81
```

...
---------------------------------------------------------------------------------------------------------------------

###  crea tu objeto Deployment, crear un Service para tu Deployment auth
```
kubectl create -f deployments/auth.yaml
kubectl create -f services/auth.yaml
```


```
kubectl create -f deployments/hello.yaml
kubectl create -f services/hello.yaml
```


```
kubectl create configmap nginx-frontend-conf --from-file=nginx/frontend.conf
kubectl create -f deployments/frontend.yaml
kubectl create -f services/frontend.yaml
```

----------------

#### Notes

### Verify your pods services
```
kubectl describe service <YOUR-SERVICE>
```

Verify resolution DNS into cluster

kubectl exec -it <pod> -- nslookup <name-service>

Verify accebility into cluster

kubectl exec -it <pod> -- curl -v <name-service>:port-number


