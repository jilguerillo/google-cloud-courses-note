### Enable Cloud Run API
```
gcloud services enable run.googleapis.com
```

### Set the compute region:
```
gcloud config set compute/region <YOUR-REGION>
```

### Create a LOCATION environment variable, example "us-east1":
```
LOCATION="us-east1"
```

### Delete the existing deployed service:
```
gcloud run services delete billing-service
```

---------------------------------------------------------------------------------------

_An example series of container images has been created by the development team. Use these images to configure and deploy within the project:_

### Deploy the payment service:
```
gcloud run deploy product-service \
   --image gcr.io/qwiklabs-resources/product-status:0.0.1 \
   --tag test1 \
   --region $LOCATION \
   --allow-unauthenticated
```

### Add the product status service to an environment variable:
```
TEST1_PRODUCT_SERVICE_URL=$(gcloud run services describe product-service --platform managed --region us-east1 --format="value(status.address.url)")
```

### Test the API is online:
```
curl $TEST1_PRODUCT_SERVICE_URL/help -w "\n"
```

### Test the product status is responding:
```
curl $TEST1_PRODUCT_SERVICE_URL/v1/revision -w "\n"
```

---------------------------------------------------------------------------------------

# Task 2. Revision tags

## Integration testing

#### Cloud Run provides the ability to deploy a new revision with redirecting traffic. A deployment of this kind is useful for integration testing of components.

### Deploy a new tagged revision (test2) with redirection of traffic:
```
gcloud run deploy product-service \
  --image gcr.io/qwiklabs-resources/product-status:0.0.2 \
  --no-traffic \
  --tag test2 \
  --region=$LOCATION \
  --allow-unauthenticated
```

### Create an environment variable for the new URL:
```
TEST2_PRODUCT_STATUS_URL=$(gcloud run services describe product-service --platform managed --region=us-east1 --format="value(status.traffic[1].url)")
```

### Test the tag revision is able to receive traffic:
```
curl $TEST2_PRODUCT_STATUS_URL/help -w "\n"
```

--------------------------

## Revision migration

### Migrate 50% of the traffic to the revision tag test2:
```
gcloud run services update-traffic product-service \
  --to-tags test2=50 \
  --region=$LOCATION
```

### Confirm the original test endpoint is distributing traffic:
```
for i in {1..10}; do curl $TEST1_PRODUCT_SERVICE_URL/help -w "\n"; done
```

--------------------------

## Tagged revision rollback

### Migrate the distributed traffic back to the test1 service:
```
gcloud run services update-traffic product-service \
  --to-tags test2=0 \
  --region=$LOCATION
```

### Test the endpoint is distributing traffic to TEST1 only:
```
for i in {1..10}; do curl $TEST1_PRODUCT_SERVICE_URL/help -w "\n"; done
```

## Traffic migration - deploy a new version

### Deploy a new tagged revision (test3) with redirection of traffic:
```
gcloud run deploy product-service \
  --image gcr.io/qwiklabs-resources/product-status:0.0.3 \
  --no-traffic \
  --tag test3 \
  --region=$LOCATION \
  --allow-unauthenticated
```

### Deploy a new tagged revision (test4) with redirection of traffic:
```
gcloud run deploy product-service \
  --image gcr.io/qwiklabs-resources/product-status:0.0.4 \
  --no-traffic \
  --tag test4 \
  --region=$LOCATION \
  --allow-unauthenticated
```
### Output a list of the revisions deployed:
```
gcloud run services describe product-service \
  --region=$LOCATION \
  --format='value(status.traffic.revisionName)'
```

### Create an environment variable for the available revisionNames:
```
LIST=$(gcloud run services describe product-service --platform=managed --region=$LOCATION --format='value[delimiter="=25,"](status.traffic.revisionName)')"=25"
```

### Split traffic between the four services using the LIST environment variable:
```
gcloud run services update-traffic product-service \
  --to-revisions $LIST --region=$LOCATION
```

### Test the endpoint is distributing traffic:
```
for i in {1..10}; do curl $TEST1_PRODUCT_SERVICE_URL/help -w "\n"; done
```

--------------------------

## Traffic splitting - update traffic between revisions

### Reset the service traffic profile to use the latest deployment:
```
gcloud run services update-traffic product-service --to-latest --platform=managed --region=$LOCATION
```

### Create an environment variable for the new URL:
```
LATEST_PRODUCT_STATUS_URL=$(gcloud run services describe product-service --platform managed --region=$LOCATION --format="value(status.address.url)")
```

### Test the tag revision is able to receive traffic:
```
curl $LATEST_PRODUCT_STATUS_URL/help -w "\n"
```

------------------------------------------------------------------------------

# Example simple cloud run service
```
REGION=us-central1
SERVICE_NAME=hello
```
```
gcloud run deploy $SERVICE_NAME \
  --allow-unauthenticated \
  --image=gcr.io/cloudrun/hello \
  --region=$REGION
```
















