# Postgres



------------------------------------------------------------------------------------------------

# Cloud SQL Proxy 

### Cloud Configuration Steps
#### Set up Secret Manager for Passwords
###### Create Secret:
```
export PASSWORD=abc12345
echo -n ${PASSWORD} | gcloud secrets create postgres-password --data-file=-

```

#### Grant Access to Service Account:
```
gcloud secrets add-iam-policy-binding postgres-password \
  --member="serviceAccount:<YOUR_SERVICE_ACCOUNT>" \
  --role="roles/secretmanager.secretAccessor"
```

#### Verify Permissions for cloudsqlproxy User
###### Make the Cloud SQL Proxy executable:
```
sudo chmod +x /home/proxy-sql/cloud_sql_proxy
```

#### Ensure cloudsqlproxy user has ownership of Cloud SQL Proxy file:
```
sudo chown cloudsqlproxy:cloudsqlproxy /home/proxy-sql/cloud_sql_proxy
```

#### Restart and Check Cloud SQL Proxy Service
###### Reload systemd and restart the service:
```
sudo systemctl daemon-reload
sudo systemctl restart cloud-sql-proxy
```

#### Check the status of Cloud SQL Proxy:
```
sudo systemctl status cloud-sql-proxy
```

------------------------------------------------------------------------------------------------

Docker cloud sql proxy 

sudo docker run -d \
  --name cloud-sql-proxy \
  --network my-network \
  -v /home/proxy-sql/sa-key.json:/sa-key.json \
  -p 5432:5432 \
  gcr.io/cloud-sql-connectors/cloud-sql-proxy:2.13.0 \
  --address=0.0.0.0 \
  --port=5432 \
  --credentials-file=/sa-key.json \
  <PROJECT_ID>:<REGION>:<INSTANCE_NAME>
