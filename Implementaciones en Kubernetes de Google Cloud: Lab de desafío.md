# Tarea 1: Crea una imagen de Docker y almacena el Dockerfile
```
source <(gsutil cat gs://cloud-training/gsp318/marking/setup_marking_v2.sh)
```

### clonar el repositorio de código fuente valkyrie-app en el directorio ~/marking. Puedes usar el siguiente comando:
```
gcloud source repos clone valkyrie-app
```

### Crea el documento valkyrie-app/Dockerfile y agrega la siguiente configuración:

```
cat > Dockerfile <<EOF
FROM golang:1.10
WORKDIR /go/src/app
COPY source .
RUN go install -v
ENTRYPOINT ["app","-single=true","-port=8080"]
EOF
```


### Usa valkyrie-app/Dockerfile para crear una imagen de Docker llamada valkyrie-prod con la etiqueta v0.0.3.
```
docker build -t valkyrie-prod:v0.0.3 .
```

-------------------------------------------------------------------------------------------------------------------
# Tarea 2: Prueba la imagen de Docker creada


### Inicia un contenedor con la imagen valkyrie-prod:v0.0.3.

- Debes asignar el puerto 8080 del host al puerto 8080 del contenedor.
- Agrega & al final del comando para que se ejecute el contenedor en segundo plano.



```
docker run -p 8080:8080 --name my-app valkyrie-prod:v0.0.2 &
```

-------------------------------------------------------------------------------------------------------------------
# Tarea 3: Envía la imagen de Docker a Artifact Registry

### Crea un repositorio llamado valkyrie-repository en Artifact Registry. Utiliza Docker como formato y usa la región para la ubicación us-central1.

```
gcloud artifacts repositories create valkyrie-docker \
    --repository-format=docker \
    --location=us-central1 \
    --async 

```

### Antes de poder enviar o extraer imágenes, debes configurar Docker para que use Google Cloud CLI con el objetivo de autenticar las solicitudes enviadas a Artifact Registry. Deberás configurar la autenticación de los repositorios de Docker en la región us-central1.

```
gcloud auth configure-docker us-central1-docker.pkg.dev

export PROJECT_ID=$(gcloud config get-value project)
```


### Vuelve a etiquetar el contenedor para poder enviarlo al repositorio. El formato debe parecerse a este: LOCATION-docker.pkg.dev/PROJECT-ID/REPOSITORY/IMAGE.
```
docker build -t us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker/valkyrie-dev:v0.0.4 .
```

### To upload an image + tag you must also create its tag
```
docker tag valkyrie-prod:v0.0.3 us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker-repo/valkyrie-prod:v0.0.3
```
### Envía la imagen de Docker a Artifact Registry.
```
docker push us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker/valkyrie-prod:v0.0.4
```

-------------------------------------------------------------------------------------------------------------------
# Tarea 4: Crea y expón una implementación en Kubernetes

### Para poder implementar la imagen en el clúster, debes tener las credenciales de Kubernetes.
```
gcloud container clusters get-credentials valkyrie-dev --zone us-central1-a
```

### Crea las implementaciones de los archivos deployment.yaml y service.yaml.
##### (2 options)
```
sed -i s#valkyrie-dev#us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-dev:v0.0.1#g k8s/deployment.yaml
```
```
sed -i s#valkyrie-dev#us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-repository/valkyrie-prod:v0.0.4#g k8s/deployment.yaml
```
> image: "us-central1-docker.pkg.dev/qwiklabs-gcp-01-0af61d3db201/valkyrie-docker/valkyrie-dev:v0.0.4"

```
kubectl create -f k8s/deployment.yaml
kubectl create -f k8s/service.yaml

```




