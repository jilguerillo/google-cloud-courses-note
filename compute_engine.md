## Compute Engine

```
gcloud compute ssh --zone <your zone> <name-instance> --project <name-project>
```

### Install wildfly in virtual machine
##### Script incialización wildfly :

```
  metadata_startup_script = <<-EOF
    #!/bin/bash
    apt-get update
    apt-get install -y openjdk-11-jdk wget unzip

    # Crear usuario y grupo 'wildfly'
    useradd -r -s /sbin/nologin wildfly

    # Descargar e instalar WildFly
    wget https://github.com/wildfly/wildfly/releases/download/28.0.0.Final/wildfly-28.0.0.Final.zip -P /tmp
    unzip /tmp/wildfly-28.0.0.Final.zip -d /opt
    ln -s /opt/wildfly-28.0.0.Final /opt/wildfly

    # Cambiar la propiedad a 'wildfly'
    chown -R wildfly:wildfly /opt/wildfly*

    # Configurar WildFly como servicio
    mkdir -p /etc/wildfly
    cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/
    cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/
    cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/

    # Ajustar permisos
    chown -R wildfly:wildfly /etc/wildfly
    chmod +x /opt/wildfly/bin/*.sh

    # Actualizar archivos de configuración
    sed -i 's@^WILDFLY_HOME=.*@WILDFLY_HOME=/opt/wildfly@' /etc/wildfly/wildfly.conf
    sed -i 's@^WILDFLY_USER=.*@WILDFLY_USER=wildfly@' /etc/wildfly/wildfly.conf
    sed -i 's@^EnvironmentFile=.*@EnvironmentFile=-/etc/wildfly/wildfly.conf@' /etc/systemd/system/wildfly.service
    sed -i 's@^ExecStart=.*@ExecStart=/opt/wildfly/bin/launch.sh@' /etc/systemd/system/wildfly.service

    # Recargar systemd y iniciar el servicio de WildFly
    systemctl daemon-reload
    systemctl enable wildfly
    systemctl start wildfly
  EOF
```

## admin wildfly
sudo /opt/wildfly/bin/jboss-cli.sh --connect
