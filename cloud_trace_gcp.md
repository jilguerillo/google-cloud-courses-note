## Preparar entorno

#### Para clonar el repositorio para la clase
```
git clone --depth=1 https://github.com/GoogleCloudPlatform/training-data-analyst
```

#### Crea un vínculo simbólico como un acceso directo al directorio de trabajo:
```
ln -s ~/training-data-analyst/courses/developingapps/v1.3/nodejs/stackdriver-trace-monitoring ~/stackdriver-trace-monitoring
```

#### Configura y ejecuta la aplicación del caso de éxito
```
cd ~/stackdriver-trace-monitoring/start
. prepare_environment.sh
```

#### Preparación del entorno de Cloud Shell para ejecutar la aplicación

```
gcloud iam service-accounts create quiz-account --display-name "Quiz Account"
gcloud iam service-accounts keys create key.json --iam-account=quiz-account@$DEVSHELL_PROJECT_ID.iam.gserviceaccount.com
export GOOGLE_APPLICATION_CREDENTIALS=key.json
```


```
gcloud projects get-iam-policy $DEVSHELL_PROJECT_ID --format json > iam.json
node setup/add_iam_policy_to_service_account.js
gcloud projects set-iam-policy $DEVSHELL_PROJECT_ID iam_modified.json
```


## Tarea 2. Aprovecha Cloud Trace

### Habilita la API de Cloud Trace
```
npm install --save @google-cloud/trace-agent
```

##### ir a frontend/app.js

----------------------------------------------------------------------------------------

```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
'use strict';

const config = require('./config');

// TODO: Carga trace-agent y, luego, inícialo
// Trace debe iniciarse antes que cualquier otro código de la
// aplicación.

require('@google-cloud/trace-agent').start({
  projectId: config.get('GCLOUD_PROJECT')
});

// END TODO

require('@google-cloud/debug-agent').start({
	allowExpressions: true
});

const path = require('path');
const express = require('express');
const scores = require('./gcp/spanner');

const {ErrorReporting} = require('@google-cloud/error-reporting');
const errorReporting = new ErrorReporting({
	projectId: config.get('GCLOUD_PROJECT')
});

const app = express();

// Static files
app.use(express.static('frontend/public/'));

app.disable('etag');
app.set('views', path.join(__dirname, 'web-app/views'));
app.set('view engine', 'pug');
app.set('trust proxy', true);
app.set('json spaces', 2);

// Questions
app.use('/questions', require('./web-app/questions'));

// Quizzes API
app.use('/api/quizzes', require('./api'));

// Display the home page
app.get('/', (req, res) => {
  res.render('home.pug');
});


// Display the Leaderboard
app.get('/leaderboard', (req, res) => {
  scores.getLeaderboard().then(scores => {
    res.render('leaderboard.pug', {
      scores
    });  
  });
});

// Use Stackdriver Error Reporting with Express
app.use(errorReporting.express);

// Basic 404 handler
app.use((req, res) => {
  res.status(404).send('Not Found');
});

// Basic error handler
app.use((err, req, res, next) => {
  /* jshint unused:false */
  console.error(err);
  // If our routes specified a specific response, then send that. Otherwise,
  // send a generic message so as not to leak anything.
  res.status(500).send(err.response || 'Something broke!');
});

if (module === require.main) {
  // Start the server
  const server = app.listen(config.get('PORT'), () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
}

module.exports = app;
```

----------------------------------------------------------------------------------------

## Iniciar applicación
```
npm start
```

----------------------------------------------------------------------------------------

##### frontend/index.js

```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const model = require('../gcp/datastore');
const publisher = require('../gcp/pubsub');

const router = express.Router();

// Automatically parse request body as JSON
router.use(bodyParser.json());

/**
 * GET /api/quizzes/:quiz
 *
 * Retrieve all the questions in the quiz.
 */
router.get('/:quiz', (req, res, next) => {

  model.list(req.params.quiz)
    .then(data => {
      res.status(200).json(data);
    }, err => { next(err) });
});


/**
 * POST /api/quizzes/:quiz
 *
 * Submit the quiz answers, return a score.
 */
router.post('/:quiz', (req, res, next) => {
  const answers = req.body; // in the form [{id, answer}]
  console.log(answers);
  model.list(req.params.quiz, false)
    .then(response => {
      const { questions } = response;

      const answersWithCorrect = answers.map(({ id, email, quiz, answer, timestamp }) => {
        const theQuestion = questions.find(q => q.id == id);
        return { id, email, quiz: theQuestion.quiz, answer, correct: theQuestion.correctAnswer, timestamp };
      });

      // Executes a set of promises in sequence
      const sequence = funcs =>
        funcs.reduce((promise, func) =>
          promise.then(result => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]));

      // Executes a set of promises in parallel
      const parallel = funcs => Promise.all(funcs.map(func => func()));

       // TODO: Sends the answers to Pub/Sub in parallel
       // Changed to parallel

       parallel(answersWithCorrect.map(answer => () => publisher.publishAnswer(answer))).then(() => {
        // Waits until all the Pub/Sub messages have been acknowledged before returning to the client
        const score = answersWithCorrect.filter(a => a.answer == a.correct).length;           
        res.status(200).json({ correct: score, total: questions.length });
      });

       // END TODO

    }, err => { next(err) });
});

/**
 * POST /api/quizzes/feedback/:quiz
 *
 * Submit the quiz feedback, get a response
 */
router.post('/feedback/:quiz', (req, res, next) => {
  const feedback = req.body; // in the form [{id, answer}]
  console.log(feedback);
  publisher.publishFeedback(feedback).then(() => {
    res.json('Feedback received');
  }).catch(err => {
    next(err);
  });

});


/**
 * Errors on "/api/questions/*" routes.
 */
router.use((err, req, res, next) => {
  // Format error and forward to generic error handler for logging and
  // responding to the request
  err.response = {
    message: err.message,
    internalCode: err.code
  };
  next(err);
});

module.exports = router;
```

-----

### See login example :
```
gcloud logging read "resource.labels.service_name=hello AND jsonPayload.message:random.txt" --format=json
```
