## Configure Workload Identity Federation with deployment pipelines 

### do you need 

```
roles/iam.workloadIdentityPoolAdmin
```

### create pool 
#### Create the workload identity pool and provider

```
 gcloud iam workload-identity-pools create YOUR-NAME-POOL \
    --location="global" \
    --description="Pool EXAMPLE" \
    --display-name="gitlab/EXAMPLE"
```


### Add a workload identity pool provider. In this case oidc:

```
gcloud iam workload-identity-pools providers create-oidc YOUR-NAME-oidc-provider \
    --location="global" \
    --workload-identity-pool="YOUR-NAME-POOL" \
    --issuer-uri="https://gitlab.com" \
    --attribute-mapping="assertion.sub" \
    --attribute-condition="assertion.namespace_id=='GROUP-ID-FROM-GITLAB'"
```
