# Tarea 1

### Clonar proyecto de prueba 
```
git clone --depth=1 https://github.com/GoogleCloudPlatform/training-data-analyst
```
```
ln -s ~/training-data-analyst/courses/developingapps/v1.3/java/firebase ~/firebase
```
```
cd ~/firebase/start
```
```
. prepare_environment.sh
```

### Ejecutar aplicación
```
mvn spring-boot:run
```
### En Identity Platform agregar script generado a la aplicación

##### Ejemplo:

```
<script src="https://www.gstatic.com/firebasejs/8.0/firebase.js"></script>
<script>
  var config = {
    apiKey: "",
    authDomain: "",
  };
  firebase.initializeApp(config);
</script>
```
