### Create a new service account called Order Initiator:
 
```
 gcloud iam service-accounts create pubsub-cloud-run-invoker \
  --display-name "Order Initiator"
```

### Confirm that the service account has been created:
```
gcloud iam service-accounts list --filter="Order Initiator"
```

### Enable the project service account to create tokens:
```
 gcloud projects add-iam-policy-binding $GOOGLE_CLOUD_PROJECT \
   --member=serviceAccount:service-$PROJECT_NUMBER@gcp-sa-pubsub.iam.gserviceaccount.com \
   --role=roles/iam.serviceAccountTokenCreator
```
### Create a new service account that will be used to trigger the services responding to Pub/Sub messages:
```
gcloud iam service-accounts create pubsub-cloud-run-invoker --display-name "PubSub Cloud Run Invoker"
```