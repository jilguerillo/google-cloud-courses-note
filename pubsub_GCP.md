# Cloud Pub/Sub

#### Crea una suscripción a Cloud Pub/Sub
```
gcloud pubsub subscriptions create cloud-shell-subscription --topic feedback
```
#### Publica un mensaje en un tema de Cloud Pub/Sub
```
gcloud pubsub topics publish feedback --message "Hello World"
```
#### Recupera un mensaje de una suscripción a Cloud Pub/Sub
```
gcloud pubsub subscriptions pull cloud-shell-subscription --auto-ack
```
#### Extraer un mensaje de cloud-shell-subscription, ejecuta el siguiente comando:
```
gcloud pubsub subscriptions pull cloud-shell-subscription --auto-ack
```

--------------------------------------------------------------------------------
# Ejemplo code publish js

```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
const config = require('../config');

// TODO: Load the Cloud Pub/Sub module

const {PubSub} = require('@google-cloud/pubsub');

// END TODO

// TODO: Create a client object against Cloud Pub/Sub
// The Pubsub(...) factory function accepts an options 
// object which is used to specify which project's Cloud 
// Pub/Sub topics should be used via the projectId 
// property. 
// The projectId is retrieved from the config module. 
// This module retrieves the project ID from the 
// GCLOUD_PROJECT environment variable.

const pubsub = new PubSub({
  projectId: config.get('GCLOUD_PROJECT')
});


// END TODO

// TODO: Get a reference to the feedback topic
// This code assumes that the topic is already present in 
// the project. 
// If it isn't then you would need to handle that case by 
// using the pubsub object's createTopic(...) method

const feedbackTopic = pubsub.topic('feedback');

// END TODO


function publishFeedback(feedback) {

  // TODO: Publish a message to the feedback topic
  // This runs asynchronously so you need to return the
  // Promise that results from executing topic.publish(...)
  // The feedback object must be converted to a buffer
  // In addition, it's a good idea to use a consistent
  // property for the message body. This lab will use the
  // name dataBuffer for the message data
  const dataBuffer=Buffer.from(JSON.stringify(feedback))
  return feedbackTopic.publish(dataBuffer);

  // END TODO
}

// The worker application will pass a callback to this 
// method as the cb argument so it is notified when a
// feedback PubSub message is received
function registerFeedbackNotification(cb) {
  // TODO: Create a subscription called worker-subscription
  // TODO: Have it auto-acknowledge messages 


    // TODO: Trap errors where the subscription already exists 
    // Create a subscription object for worker-subscription if
    // the subscrioption already exists
    // err.code == 6 means subscription already exists 

    // END TODO

    // TODO: Use the get() method on the subscription object to call 
    // the API request to return a promise


      // The results argument in the promise is an array - the 
      // first element in this array is the subscription object.

      // TODO: Declare a subscription constant


      // END TODO

      // TODO: Register an event handler for message events
      // Use an arrow function to handle the event
      // When a message arrives, invoke a callback


      // END TODO


      // TODO: Register an event handler for error events
      // Print the error to the console


      // END TODO


    // END TODO for the get() method promise 

    // TODO
    // Add a final catch to the promise to handle errors


    // END TODO


  // END TODO for the create subscription method

}

// [START exports]
module.exports = {
  publishFeedback,
  registerFeedbackNotification
};
// [END exports]
```


# Código para usar la funcionalidad de publicación de Pub/Sub
--------------------------------------------------------------------------------
```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
const config = require('../config');

// TODO: Load the Cloud Pub/Sub module

const {PubSub} = require('@google-cloud/pubsub');

// END TODO

// TODO: Create a client object against Cloud Pub/Sub
// The Pubsub(...) factory function accepts an options 
// object which is used to specify which project's Cloud 
// Pub/Sub topics should be used via the projectId 
// property. 
// The projectId is retrieved from the config module. 
// This module retrieves the project ID from the 
// GCLOUD_PROJECT environment variable.

const pubsub = new PubSub({
  projectId: config.get('GCLOUD_PROJECT')
});


// END TODO

// TODO: Get a reference to the feedback topic
// This code assumes that the topic is already present in 
// the project. 
// If it isn't then you would need to handle that case by 
// using the pubsub object's createTopic(...) method

const feedbackTopic = pubsub.topic('feedback');

// END TODO


function publishFeedback(feedback) {

  // TODO: Publish a message to the feedback topic
  // This runs asynchronously so you need to return the
  // Promise that results from executing topic.publish(...)
  // The feedback object must be converted to a buffer
  // In addition, it's a good idea to use a consistent
  // property for the message body. This lab will use the
  // name dataBuffer for the message data
  const dataBuffer=Buffer.from(JSON.stringify(feedback))
  return feedbackTopic.publish(dataBuffer);

  // END TODO
}

// The worker application will pass a callback to this
// method as the cb argument so it is notified when a
// feedback PubSub message is received
function registerFeedbackNotification(cb) {

  // TODO: Create a subscription called worker-subscription
  // TODO: Have it auto-acknowledge messages

  feedbackTopic.createSubscription('worker-subscription', { autoAck: true }, (err, feedbackSubscription) => {

    // TODO: Trap errors where the subscription already exists
    // Create a subscription object for worker-subscription if
    // the subscription already exists
    // err.code == 6 means subscription already exists
    if (err && err.code == 6) {
        // subscription already exists
        console.log("Feedback subscription already exists");
        feedbackSubscription=feedbackTopic.subscription('worker-subscription')
    }
    // END TODO


    // TODO: Use the get() method on the subscription object to call
    // the API request to return a promise
    feedbackSubscription.get().then(results => {

      // The results argument in the promise is an array - the
      // first element in this array is the subscription object.

      // TODO: Declare a subscription constant
      const subscription = results[0];

      // END TODO

      // TODO: Register an event handler for message events
      // Use an arrow function to handle the event
      // When a message arrives, invoke a callback
      subscription.on('message', message => {
          cb(message.data);
      });

      // END TODO

      // TODO: Register an event handler for error events
      // Print the error to the console
      subscription.on('error', err => {        
          console.error(err);
      });

      // END TODO

    })
    // END TODO for the get() method promise

    // TODO
    // Add a final catch to the promise to handle errors
    .catch(error => { console.log("Error getting feedback subscription", error)});

    // END TODO

  });
  // END TODO for the create subscription method
}
// [START exports]
module.exports = {
  publishFeedback,
  registerFeedbackNotification
};
// [END exports]

```

# Código para usar la funcionalidad de suscripción a Pub/Sub en Worker
--------------------------------------------------------------------------------
```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// TODO: Load the ../server/gcp/pubsub module
const subscriber = require('../server/gcp/pubsub');

// END TODO

// TODO: Load the ../server/gcp/languageapi module
const languageAPI = require('../server/gcp/languageapi');

// END TODO

// TODO: Load the ../server/gcp/spanner module
const feedbackStorage = require('../server/gcp/spanner');

// END TODO

// The callback function - invoked when a message arrives
function handler(message) {
  console.log('Message received');

  // TODO: Log the message to the console
  var messageData = JSON.parse(message.toString());
  console.log(messageData);
  // END TODO


  // TODO: Invoke the languageapi module method
  // with the feedback from the student
  languageAPI.analyze(messageData.feedback)
  .then(score => {
    // TODO: Log sentiment score
    console.log(`Score: ${score}`);

    // END TODO

    // TODO: Add a score property to feedback object
    messageData.score = score;
    return messageData;
  })
  // TODO: Pass on the feedback object
  // to next Promise handler
  .then(feedbackStorage.saveFeedback)

  // END TODO

  // TODO: Add third .then(...)
  .then(() => {
      // TODO Log feedback saved message
      console.log('Feedback saved');

      // END TODO
  })
  // END TODO
  // TODO close off the promise with a catch and log
  // any errors
  .catch(console.error);

  // END TODO

}

// TODO: Register the callback with the module
subscriber.registerFeedbackNotification(handler);
// END TODO

```

--------------------------------------------------------------------------------

#### Corre worker
```
npm run worker
```
#### Spanner client for save message
--------------------------------------------------------------------------------
```
// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


// Import the config module
const config = require('../config');

// TODO: Import the @google-cloud/spanner module

const {Spanner} = require('@google-cloud/spanner');

// END TODO

// TODO: Create a client object to access Cloud Spanner
// The Spanner(...) factory function accepts an options
// object which is used to select which project's Cloud
// Spanner database instance(s) should be used via the
// projectId property.
// The projectId is retrieved from the config module.
// This module retrieves the project ID from the
// GCLOUD_PROJECT environment variable.
const spanner = new Spanner({
    projectID: config.get('GCLOUD_PROJECT')
});

// END TODO

// TODO: Get a reference to the Cloud Spanner instance
const instance = spanner.instance('quiz-instance');

// END TODO

// TODO: Get a reference to the Cloud Spanner database
const database = instance.database('quiz-database');

// END TODO

// TODO: Get a reference to the Cloud Spanner table
const feedbackTable = database.table('feedback');

// END TODO



async function saveFeedback(
    { email, quiz, timestamp, rating, feedback, score }) {

    // TODO: Declare rev_email constant
    // TODO: Produce a 'reversed' email address
    // eg app.dev.student@example.org -> org_example_student_dev_app
    const rev_email = email
        .replace(/[@\.]/g, '_')
        .split('_')
        .reverse()
        .join('_');

    // END TODO

    // TODO: Create record object to be inserted into Spanner
    const record = {
        feedbackId: `${rev_email}_${quiz}_${timestamp}`,
        email,
        quiz,
        timestamp,
        rating,
        score: Spanner.float(score),
        feedback,
    };
    // END TODO

    // TODO: Insert the record into the table
    // use try {} catch {} and check for err.code==6 to trap
    // insert errors caused by duplicated PubSub messages
    try {
        console.log('Saving feedback');
        await feedbackTable.insert(record);
    } catch (err) {
        if (err.code === 6 ) {
            console.log("Duplicate message - feedback already saved");
        } else {
            console.error('ERROR processing feedback:', err);
        }
    }
    // END TODO

}

module.exports = {
    saveFeedback
};
```
--------------------------------------------------------------------------------