[** Credits Lab **](https://codelabs.developers.google.com/codelabs/cloud-run-events#5)

# Eventarc

### Enable required services for Eventarc:
```
gcloud services enable eventarc.googleapis.com
```
### You also need a service account to be used by triggers. Create a service account:
```
SERVICE_ACCOUNT=eventarc-trigger-sa
```
```
gcloud iam service-accounts create $SERVICE_ACCOUNT
```
### To see the list of different types of events:
```
gcloud beta eventarc attributes types list
```
### To get more information about each event type:
```
gcloud beta eventarc attributes types describe google.cloud.audit.log.v1.written
```
### To see the list of services that emit a certain event type:
```
gcloud beta eventarc attributes service-names list --type=google.cloud.audit.log.v1.written
```
### To see the list of method names (sub-events) that each service can emit:
```
gcloud beta eventarc attributes method-names list --type=google.cloud.audit.log.v1.written --service-name=workflows.googleapis.com
```
----------------------------------------------------------------------------------------------------------------

## Examples implementation gcp source events -> eventarc -> cloud run

### Setup project ID:

```
PROJECT_ID=your-project-id \
REGION=us-central1 \
SERVICE_NAME=cloud-run-service \
TRIGGER_NAME=trigger-pubsub \
TOPIC_ID_EXISTING=eventarc-topic \
TRIGGER_NAME_EXISTING=trigger-pubsub-existing \
SERVICE_ACCOUNT=eventarc-trigger-sa
```

```
gcloud config set project $PROJECT_ID
```
### Create cloud run service. Deploy the hello container to Cloud Run:
```
gcloud run deploy $SERVICE_NAME \
  --allow-unauthenticated \
  --image=gcr.io/cloudrun/hello \
  --region=$REGION
```
### Enable required services for Eventarc:
```
gcloud services enable eventarc.googleapis.com
```
### Create a service account to be used by all triggers:
```
gcloud iam service-accounts create $SERVICE_ACCOUNT
```
### Create a trigger to filter events published to the Pub/Sub topic to our deployed Cloud Run service:
```
gcloud eventarc triggers create $TRIGGER_NAME \
  --destination-run-service=$SERVICE_NAME \
  --destination-run-region=$REGION \
  --event-filters="type=google.cloud.pubsub.topic.v1.messagePublished" \
  --location=$REGION \
  --service-account=$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```
### Test
```
TOPIC_ID=$(gcloud eventarc triggers describe $TRIGGER_NAME --location $REGION --format='value(transport.pubsub.topic)')
```
```
gcloud pubsub topics publish $TOPIC_ID --message="Hello World"
```

### Using an existing topic

```
gcloud pubsub topics create $TOPIC_ID_EXISTING
```
```
gcloud eventarc triggers create $TRIGGER_NAME_EXISTING \
  --destination-run-service=$SERVICE_NAME \
  --destination-run-region=$REGION \
  --event-filters="type=google.cloud.pubsub.topic.v1.messagePublished" \
  --location=$REGION \
  --transport-topic=projects/$PROJECT_ID/topics/$TOPIC_ID \
  --service-account=$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```
### Test
```
gcloud pubsub topics publish $TOPIC_ID --message="Hello again"
```

