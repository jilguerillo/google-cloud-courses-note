## Configura la región de tu proyecto:
```
gcloud config set compute/region < your-ZONE >
```

### Habilita las aplicaciones de App Engine:
```
gcloud app create --region=< your-ZONE >
```

### Crea una base de datos de Firestore:
```
gcloud firestore databases create --region=< your-ZONE >
```

##### Check and create your project https://console.firebase.google.com/

-------------------------------------------------------------------------------------

## Usa el IDE para conectarte a Firebase y, luego, implementa tu aplicación. Escribe los comandos en la terminal disponible en el editor.

### Usa este comando para autenticarte en Firebase:
```
firebase login --no-localhost
```

### Inicializa un nuevo proyecto de Firebase en tu directorio de trabajo actual:
```
firebase init
```

### Implementar tu aplicación de Firebase:
```
firebase deploy
```

-------------------------------------------------------------------------------------

## Example migration bucket to Firestore

### Migrate the import files into a Cloud Storage bucket that has been created for you:
```
gsutil mb -c standard -l Region gs://$GOOGLE_CLOUD_PROJECT-customer
```

```
gsutil cp -r gs://spls/gsp645/2019-10-06T20:10:37_43617 gs://$GOOGLE_CLOUD_PROJECT-customer
```

### Import this data into Firebase:
```
gcloud beta firestore import gs://$GOOGLE_CLOUD_PROJECT-customer/2019-10-06T20:10:37_43617/
```














