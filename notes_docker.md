# Docker File Notes

### Ejecutar un contenedor de Hello World
```
docker run hello-world
```

### Consultar la imagen del contenedor que se extrajo de Docker Hub
```
docker images
```

### Comando siguiente para observar los contenedores que están en ejecución
```
docker ps
```

### Ver todos los contenedores, incluso los que terminaron de ejecutarse
```
docker ps -a
```

------------------------------------------------------------------------------------------------

### Crear DockerFile example
```
cat > Dockerfile <<EOF
### Use an official Node runtime as the parent image
FROM node:lts

### Set the working directory in the container to /app
WORKDIR /app

### Copy the current directory contents into the container at /app
ADD . /app

### Make the container's port 80 available to the outside world
EXPOSE 80

### Run app.js using node when the container launches
CMD ["node", "app.js"]
EOF
```
------------------------------------------------------------------------------------------------

### Crear la aplicación del nodo

```

cat > app.js <<EOF
const http = require('http');

const hostname = '0.0.0.0';
const port = 80;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World\n');
});

server.listen(port, hostname, () => {
    console.log('Server running at http://%s:%s/', hostname, port);
});

process.on('SIGINT', function() {
    console.log('Caught interrupt signal and will exit');
    process.exit();
});
EOF

```

------------------------------------------------------------------------------------------------

### Comando desde el directorio en el que se encuentra el Dockerfile
```
docker build -t node-app:0.1 .
```

### Ejecutar contenedores basados en la imagen que compilaste
```
docker run -p 4000:80 --name my-app node-app:0.1
```
### Comando siguiente para detener y quitar el contenedor
```
docker stop my-app && docker rm my-app
```

### Comando siguiente para iniciar el contenedor en segundo plano
```
docker run -p 4000:80 --name my-app -d node-app:0.1

docker ps
```
### Detén y quita todos los contenedores
```
docker stop $(docker ps -q)
docker rm $(docker ps -aq)
```

### Comando para quitar todas las imágenes de Docker
```
docker rmi us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
docker rmi node:lts
docker rmi -f $(docker images -aq) # remove remaining images
docker images
```
### Extrae la imagen y ejecútala
```
docker pull us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
docker run -p 4000:80 -d us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
curl http://localhost:4000
```

### Detén y quita todos los contenedores:
```
docker stop $(docker ps -q)
docker rm $(docker ps -aq)
```
------------------------------------------------------------------------------------------------

## GCP 

### configurar la autenticación en los repositorios de Docker de la región (la region que elegiste en el repositorios de artifacts) , ejecuta el comando
```
gcloud auth configure-docker us-central1-docker.pkg.dev
```
### comandos siguientes para establecer el ID de tu proyecto
```
export PROJECT_ID=$(gcloud config get-value project)
```

### docker build -t us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2 .
```
docker build -t us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2 .
```

### Envía esta imagen a Artifact Registry
```
docker push us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
```

## Extrae la imagen y ejecútala.
```
docker pull us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
docker run -p 4000:80 -d us-central1-docker.pkg.dev/$PROJECT_ID/my-repository/node-app:0.2
curl http://localhost:4000
```
------------------------------------------------------------------------------------------------

# Examples

## Example 1 for run container

```
docker run -d \
  --name <name_service_running> \
  --network <NETWORK_NAME> \
  -p <PORT_EXAMPLE_8082:8082> \
  -e DB_HOST=<NAME_HOST_DB> \
  -e DB_PORT=<PORT_DB> \
  -e DB_USER=<USER_DB> \
  -e DB_PASSWORD=${DB_PASSWORD} \
  <NAME_IMAGEN_DOCKER:latest>
```

## Example 2

### Pull and Run Docker Containers
#### Pull and start service
```
docker pull us-central1-docker.pkg.dev/<PROJECT_ID>/<REPO_NAME>/<IMAGE_NAME>:<IMAGE_TAG> &&
docker stop <SERVICE_NAME> || true &&
docker rm <SERVICE_NAME> || true &&
docker run -d --name <SERVICE_NAME> -p 8081:8080 -e DB_PASSWORD=$DB_PASSWORD us-central1-docker.pkg.dev/<PROJECT_ID>/<REPO_NAME>/<IMAGE_NAME>:<IMAGE_TAG>
```

### Pull and start  service
```
sudo docker run -d --name <SERVICE_NAME> -p 8080:8080 -e DB_PASSWORD=<PASSWORD> us-central1-docker.pkg.dev/<PROJECT_ID>/<REPO_NAME>/<IMAGE_NAME>@<IMAGE_SHA>
```

### Docker Pull for Specific Image
```
sudo docker pull us-central1-docker.pkg.dev/<PROJECT_ID>/<REPO_NAME>/<IMAGE_NAME>@<IMAGE_SHA>
```

------------------------------------------------------------------------------------------------

# Docker network 

### Create Docker Network
```
docker network create my-network
```
> Creates an isolated network for Docker containers to communicate.

## Run image with network 
```
docker run -d \
  --name <name_service_running> \
  --network <NETWORK_NAME> \
  -p <PORT_EXAMPLE_8082:8082> \
  <NAME_IMAGEN_DOCKER:latest>
```
