# Bash

### Verify post in use
```
sudo lsof -i :<PORT>
```

#### Stop service and free port
```
sudo fuser -k <PORT>/tcp
```

#### Check permissions for a user
##### Verify that a specific user has access to a file or directory:
```
sudo chown <USER>:<USER_GROUP> /path/to/file
```

#### Reload and restart a service
##### Reload systemd and restart a service:

```
sudo systemctl daemon-reload
sudo systemctl restart <service-name>
```

#### Check service status
```
sudo systemctl status <service-name>
```
> Check the status of a specific service:


# Wildfly
#### Panel wildfly connect
```
sudo /opt/wildfly/bin/jboss-cli.sh --connect
```

# Maven

#### Example how install depedendency jar in local maven
```
mvn install:install-file -Dfile=<depedendency>.jar  -DgroupId=com.example  -DartifactId=<depedendencyaExample>  -Dversion=1.0 -Dpackaging=jar
```


# Quick Command Descriptions

> systemctl: Command to control the system’s systemd services (start, stop, enable, disable).

> daemon-reload: Reloads the systemd manager configuration, useful after changing service files.