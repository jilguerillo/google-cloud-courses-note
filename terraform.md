# Terraform for google basic 

[Registry.terraform.io](https://registry.terraform.io/namespaces/hashicorp)

```
terraform --version
```
## Add Google Cloud provider

### Create the main.tf file:
```
touch main.tf
```
### Copy the following code in the main.tf file.

```
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {

  project = "your-project"
  region  = "your-region"
  zone    = "your-zone-a"
}
```

```
terraform init
```
## Build the infrastructure

```
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {

  project = "your-project"
  region  = "your-region"
  zone    = "your-zone-a"
}

resource "google_compute_instance" "terraform" {
  name         = "terraform"
  machine_type = "e2-micro"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
}
```


#### is OK? :
```
terraform plan
```
```
terraform apply
```
## Change the infrastructure

##### Plan to change

1. -    Adding network tags
2. -   Editing the machine-type


### Add a tags argument to the instance in main.tf:

```
resource "google_compute_instance" "terraform" {
  name         = "terraform"
  machine_type = "e2-micro"
  tags         = ["web", "dev"]
  # ...
}
```

```
terraform plan
```
```
terraform apply
```

### Editing the machine type

### change type machine in main.tf:
```
machine_type = "e2-medium"
```
### add in main.tf:
```
allow_stopping_for_update = true
```
##### final block:

```
resource "google_compute_instance" "terraform" {
  name         = "terraform"
  machine_type = "e2-medium"

  tags         = ["web", "dev"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  allow_stopping_for_update = true
}
```

```
terraform plan
```
```
terraform apply
```
## Destroy the infrastructure

### Execute the following command. Answer yes to execute this plan and destroy the infrastructure:
```
terraform destroy
```
------------------------------------------------------------------------------------

##### See graph
```
terraform graph | dot -Tsvg > graph.svg
```
#### Para volver a escribir los archivos de configuración de Terraform en un formato y estilo canónicos, ejecuta el siguiente comando:
```
terraform fmt
```