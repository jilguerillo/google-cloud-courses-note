# Notas de Estudio de Google Cloud Platform (GCP)

## Descripción

Este repositorio contiene mis **notas de estudio** para la certificación **Google Cloud Associate**. Estas notas han sido cuidadosamente organizadas y actualizadas para ayudarte a prepararte eficazmente para obtener la certificación de Associate en Google Cloud Platform.

## Migración y Actualizaciones

Las notas han sido **migradas** y se encuentran disponibles en el siguiente enlace:

🔗 [Notas de Google Cloud en GitLab](https://gitlab.com/studies-group-v/cloud-v/cloud-v-1/-/tree/main/providers/gcp)

Las actualizaciones futuras se realizarán directamente en este repositorio. Además, planeo incluir notas de otros **proveedores de servicios en la nube**, ampliando así el alcance de este recurso educativo.

## Licencia

Este proyecto está licenciado bajo la **GNU Affero General Public License v3.0**. Puedes consultar la [licencia completa](LICENSE) para más detalles.
