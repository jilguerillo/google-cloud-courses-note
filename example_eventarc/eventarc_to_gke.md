# Implement eventarc with GKE

## Setup GKE

```
gcloud auth list
```
```
export PROJECT_ID=$(gcloud config get-value project)
```
```
echo $PROJECT_ID
```


### Enable api container if necessary and eventarc
```
gcloud services enable container.googleapis.com \
eventarc.googleapis.com
cloudresourcemanager.googleapis.com

```
### Create an Autopilot GKE cluster:
```
CLUSTER_NAME=<"name-your-cluster"> \
REGION=<"your-region">
```
```
gcloud container clusters create-auto $CLUSTER_NAME --region $REGION
```
### After that your cluster created get authentication credentials to interact with the cluster with kubectl:

```
gcloud container clusters get-credentials $CLUSTER_NAME \
    --region $REGION
```
### Deploy Cloud Run's [hello container](https://github.com/GoogleCloudPlatform/cloud-run-hello/tree/master) as a Kubernetes deployment on GKE. This service logs received HTTP requests and CloudEvents:
```
SERVICE_NAME=hello-gke
```
```
kubectl create deployment $SERVICE_NAME \
    --image=gcr.io/cloudrun/hello
```

##### If you check the Dockerfile of the hello container you can see ENTRYPOINT ["/server"]

### Expose the deployment as an internal Kubernetes service. This creates a service with a stable IP accessible within the cluster:
```
kubectl expose deployment $SERVICE_NAME \
  --type ClusterIP --port 80 --target-port 8080
```

### Now verify if the pod is running and has the external IP
```
kubectl get pods
```
```
kubectl get svc
```

## Setup Eventarc

### Enable Eventarc to manage GKE clusters:
```
gcloud eventarc gke-destinations init
```

##### In the previous step you will be asked if you want to assign roles to the service account. These roles are required.


> To use Eventarc with Cloud Run for Anthos/GKE destinations, Eventarc Service Agent [service-12345example@gcp-sa-eventarc.iam.gserviceaccount.com] needs to be bound to the following required roles:
> - roles/compute.viewer
> - roles/container.developer
> - roles/iam.serviceAccountAdmin
> 
> Would you like to bind these roles? (y/N)?

###### Eventarc creates a separate Event Forwarder pod for each trigger targeting a GKE service, and requires explicit permissions to make changes to the cluster. This is done by granting permissions to a special service account to manage resources in the cluster. This needs to be done once per Google Cloud project.


[** Credits Lab **](https://codelabs.developers.google.com/codelabs/eventarc-events-gke#3)
