### Setup project ID:

```
PROJECT_ID=your-project-id \
REGION=us-central1 \
SERVICE_NAME=cloud-run-service \
SERVICE_ACCOUNT=eventarc-trigger-sa
```
```
gcloud config set project $PROJECT_ID
```

### Enable required services for Eventarc:
```
gcloud services enable eventarc.googleapis.com
```

### Create cloud run service. Deploy the hello container to Cloud Run:
```
gcloud run deploy $SERVICE_NAME \
  --allow-unauthenticated \
  --image=gcr.io/cloudrun/hello \
  --region=$REGION
```

### Create a service account to be used by all triggers:
```
gcloud iam service-accounts create $SERVICE_ACCOUNT
```

### Grant the eventarc.eventReceiver role, so the service account can be used in a Cloud Storage trigger:
```
gcloud projects add-iam-policy-binding $PROJECT_ID \
  --role roles/eventarc.eventReceiver \
  --member serviceAccount:$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```

###  Add the pubsub.publisher role to the Cloud Storage service account for Cloud Storage triggers:
```
SERVICE_ACCOUNT_STORAGE=$(gsutil kms serviceaccount -p $PROJECT_ID)
```
```
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT_STORAGE \
    --role roles/pubsub.publisher
```

### Create bucket

```
BUCKET_NAME_1=eventarc-gcs-1-$PROJECT_ID \
BUCKET_NAME_2=eventarc-gcs-2-$PROJECT_ID \
BUCKET_NAME_3=eventarc-gcs-3-$PROJECT_ID
```
```
gsutil mb -l $REGION gs://$BUCKET_NAME_1 
```
```
gsutil mb -l $REGION gs://$BUCKET_NAME_2 
```
```
gsutil mb -l $REGION gs://$BUCKET_NAME_3
```

### Create triggers for all bucket. One triggers per bucket
```
TRIGGER_NAME_1=trigger-storage-1 \
TRIGGER_NAME_2=trigger-storage-2 \
TRIGGER_NAME_3=trigger-storage-3
```

```
gcloud eventarc triggers create $TRIGGER_NAME_1 \
  --destination-run-service=$SERVICE_NAME \
  --destination-run-region=$REGION \
  --event-filters="type=google.cloud.storage.object.v1.finalized" \
  --event-filters="bucket=$BUCKET_NAME_1" \
  --location=$REGION \
  --service-account=$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```

```
gcloud eventarc triggers create $TRIGGER_NAME_2 \
  --destination-run-service=$SERVICE_NAME \
  --destination-run-region=$REGION \
  --event-filters="type=google.cloud.storage.object.v1.finalized" \
  --event-filters="bucket=$BUCKET_NAME_2" \
  --location=$REGION \
  --service-account=$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```

```
gcloud eventarc triggers create $TRIGGER_NAME_3 \
  --destination-run-service=$SERVICE_NAME \
  --destination-run-region=$REGION \
  --event-filters="type=google.cloud.storage.object.v1.finalized" \
  --event-filters="bucket=$BUCKET_NAME_3" \
  --location=$REGION \
  --service-account=$SERVICE_ACCOUNT@$PROJECT_ID.iam.gserviceaccount.com
```

### Verify created triggers:
```
gcloud eventarc triggers list
```

### Testing

#### Creates files
```
echo "Hello World" > random_1.txt
```
```
echo "Hello World" > random_2.txt
```
```
echo "Hello World" > random_3.txt
```
#### Uploads file to buckets
```
gsutil cp random_1.txt gs://$BUCKET_NAME_1/random_1.txt
```
```
gsutil cp random_2.txt gs://$BUCKET_NAME_1/random_2.txt
```
```
gsutil cp random_3.txt gs://$BUCKET_NAME_1/random_3.txt
```

## See logs in CLoud Run



