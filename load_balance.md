# Load Balance

## Stress test

### Create a VM (Debian) and execute in SSH. IP LOAD BALANCE LB_IP_v4
```
export LB_IP=<Enter your [LB_IP_v4] here>
```

```
echo $LB_IP
```

```
ab -n 500000 -c 1000 http://$LB_IP/
```